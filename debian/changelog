octave-general (2.1.1-2) unstable; urgency=medium

  * d/control: Bump debhelper compatibility level to 13
  * d/u/metadata: New file
  * d/p/typo-docstring-mark-for-deletion.patch: New patch

 -- Rafael Laboissière <rafael@debian.org>  Tue, 28 Jul 2020 08:17:25 -0300

octave-general (2.1.1-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * New upstream version 2.1.1
  * d/copyright: fix license info for Makefile and AppStream file
  * debian/clean: add various generated files

  [ Rafael Laboissière ]
  * d/control: Bump Standards-Version to 4.5.0 (no changes needed)

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 14 Mar 2020 09:37:53 +0100

octave-general (2.1.0-3) unstable; urgency=medium

  * d/control:
    + Bump Standards-Version to 4.4.1 (no changes needed)
    + Bump dependency on dh-octave to >= 0.7.1
      This allows the injection of the virtual package octave-abi-N
      into the package's list of dependencies.

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 04 Nov 2019 13:33:03 -0300

octave-general (2.1.0-2) unstable; urgency=medium

  * d/control:
    + Add Rules-Requires-Root: no
    + Bump Standards-Version to 4.3.0
    + Bump to debhelper compat level 12
  * Build-depend on debhelper-compat instead of using d/compat

 -- Rafael Laboissiere <rafael@debian.org>  Wed, 02 Jan 2019 22:55:39 -0200

octave-general (2.1.0-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * New upstream version 2.1.0
    The SHA1 function is now working (Closes: #839003)
  * d/copyright: Reflect upstream changes
  * d/control:
    + Bump Standards-Version to 4.1.4 (no changes needed)
    + Build-depends on nettle-dev and pkg-config (needed for SHA1.cc)
  * d/s/options: Add extend-diff-ignore list

  [ Mike Miller ]
  * d/control, d/copyright: Use secure URL for upstream source.

 -- Rafael Laboissiere <rafael@debian.org>  Mon, 14 May 2018 13:21:58 -0300

octave-general (2.0.0-3) unstable; urgency=medium

  * Use dh-octave for building the package
  * d/control:
     + Use Debian's GitLab URLs in Vcs-* headers
     + Change Maintainer to team+pkg-octave-team@tracker.debian.org

 -- Rafael Laboissiere <rafael@debian.org>  Sat, 10 Feb 2018 07:30:37 -0200

octave-general (2.0.0-2) unstable; urgency=medium

  [ Sébastien Villemot ]
  * d/copyright: use secure URL for format.
  * d/watch: bump to format version 4.

  [ Rafael Laboissiere ]
  * Use the dh-based version of octave-pkg-dev
  * Set debhelper compatibility level to >= 11
    + Bump Standards-Version to 4.1.3 (no changes needed)
    + Use secure URIs in the Vcs-* fields
    + Use cgit instead of gitweb in Vcs-Browser URL
    + Add Testsuite field

 -- Rafael Laboissiere <rafael@debian.org>  Fri, 29 Dec 2017 22:13:34 -0200

octave-general (2.0.0-1) unstable; urgency=medium

  [ Rafael Laboissiere ]
  * Imported Upstream version 2.0.0
  * d/p/autoload-yes.patch: Remove patch (deprecated upstream)
  * Build-depend on octave-pkg-dev >> 1.2.0 (for Octave 4.0 transition)
  * Drop the inputParser function, which is now in Octave 4.0 core
  * Remove code for dropping the inputParser function

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 18 Jul 2015 23:24:19 +0200

octave-general (1.3.4-2) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Uploaders: remove Thomas Weber, add Rafael Laboissiere.

  [ Rafael Laboissiere ]
  * d/copyright: Ensure that all files have copyright information
  * Bump Standards-Version to 3.9.6 (no changes needed)

 -- Rafael Laboissiere <rafael@laboissiere.net>  Sat, 27 Sep 2014 06:22:22 -0300

octave-general (1.3.4-1) unstable; urgency=medium

  [ Sébastien Villemot ]
  * Imported Upstream version 1.3.4
  * debian/copyright: reflect upstream changes.
  * Build-Depends on octave-pkg-dev >= 1.1.0.
    This version needs Octave 3.8 to get compiled, it does not compile against
    Octave 3.6 (despite what's indicated in DESCRIPTION).

  [ Thomas Weber ]
  * debian/control: Use canonical URLs in Vcs-* fields

  [ Rafael Laboissiere ]
  * Bump to Standards-Version 3.9.5, no changes needed
  * Drop Lintian override for hardening-no-fortify-functions

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 20 Feb 2014 16:38:52 +0100

octave-general (1.3.2-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * Bump Standards-Version to 3.9.4 (no changes needed)
  * Use Sébastien Villemot's @debian.org email address
  * debian/copyright: Use the octave-maintainers mailing list as upstream
    contact
  * Remove obsolete DM-Upload-Allowed flag

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 20 May 2013 13:35:39 +0200

octave-general (1.3.2-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.2
  * debian/copyright: Reflect upstream changes
  * Add Lintian override for hardening-no-fortify-functions warnings

 -- Thomas Weber <tweber@debian.org>  Mon, 06 Aug 2012 22:08:52 +0200

octave-general (1.3.1-1) unstable; urgency=low

  * Imported Upstream version 1.3.1

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Thu, 17 May 2012 22:09:13 +0200

octave-general (1.3.0-1) unstable; urgency=low

  [ Rafael Laboissiere ]
  * Imported Upstream version 1.3.0
  * Drop useless patch fix-tests
  * debian/watch: Use the SourceForge redirector
  * debian/copyright: Adjust for the new upstream version
  * debian/patches/autoload-yes.patch: New patch

 -- Sébastien Villemot <sebastien.villemot@ens.fr>  Sat, 31 Mar 2012 09:50:30 +0200

octave-general (1.2.2-3) unstable; urgency=low

  [ Sébastien Villemot ]
  * Remove Iulian Udrea from Uploaders, with his consent

  [ Rafael Laboissiere ]
  * Bump to debhelper compat level 9
  * Build-depend on octave-pkg-dev >= 1.0.0, to build against Octave 3.6
  * Bump to Standards-Version 3.9.3, no changes needed
  * Add Sébastien Villemot to the list of Uploaders
  * debian/copyright: update to machine-readable format 1.0

 -- Thomas Weber <tweber@debian.org>  Tue, 13 Mar 2012 20:57:52 +0100

octave-general (1.2.2-2) unstable; urgency=low

  * Upload to unstable
  * New patch: fix-tests, fixes failing tests due to type checking

 -- Thomas Weber <tweber@debian.org>  Sun, 17 Apr 2011 18:26:00 +0200

octave-general (1.2.2-1) experimental; urgency=low

  [ Iulian Udrea ]
  * New upstream release.
  * debian/control:
    - Add myself to Uploaders.
    - Bump Standards-Version to 3.9.1 (no changes required).

 -- Thomas Weber <tweber@debian.org>  Sun, 17 Oct 2010 22:16:10 +0200

octave-general (1.2.1-1) unstable; urgency=low

  * New upstream release

 -- Thomas Weber <tweber@debian.org>  Mon, 17 May 2010 22:14:11 +0200

octave-general (1.2.0-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Remove Rafael Laboissiere from Uploaders (Closes: #571902)
    - Remove Ólafur Jens Sigurðsson <ojsbug@gmail.com> from Uploaders
  * Switch to dpkg-source 3.0 (quilt) format
  * Standards-Version: Bump to 3.8.4 (no changes needed)
  * Gunzip and regzip the original tarball. It contains some superfluous bits,
    resulting in errors
    http://sourceforge.net/apps/trac/sourceforge/ticket/9948)

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 25 Apr 2010 18:27:03 +0200

octave-general (1.1.3-2) unstable; urgency=low

  * Upload to unstable.

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Fri, 04 Dec 2009 00:37:35 +0100

octave-general (1.1.3-1) experimental; urgency=low

  [ Rafael Laboissiere ]
  * New upstream release
  * debian/control:
    + (Standards-Version): Bump to 3.8.1 (no changes needed)
    + (Depends): Add ${misc:Depends}
    + (Vcs-Git, Vcs-Browser): Adjust to new Git repository
    + Build-depend on octave-pkg-dev >= 0.7.0, such that the
      package is built against octave3.2
  * debian/copyright:
    + Use DEP5 URL in Format-Specification
    + Use separate License stanzas for instructing about the location of
      the different licenses used in the package

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sun, 19 Jul 2009 14:17:39 +0200

octave-general (1.0.7-2) unstable; urgency=low

  [ Rafael Laboissiere ]
  * debian/copyright: Add header
  * debian/control: Bump build-dependency on octave-pkg-dev to >= 0.6.4,
    such that the package is built with the versioned packages directory

  [ Thomas Weber ]
  * Upload to unstable

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sat, 04 Apr 2009 22:24:42 +0200

octave-general (1.0.7-1) experimental; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * debian/control: Bumped Standards-Version to 3.8.0 (no changes
    needed)

  [ Thomas Weber ]
  * New upstream release
  * Bump dependency on octave-pkg-dev to 0.6.1, to get the experimental
    version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Sat, 06 Dec 2008 20:44:56 +0100

octave-general (1.0.6-1) unstable; urgency=low

  [ Ólafur Jens Sigurðsson ]
  * New upstream version

 -- Thomas Weber <thomas.weber.mail@gmail.com>  Mon, 12 May 2008 08:24:47 +0200

octave-general (1.0.5-1) unstable; urgency=low

  * Initial release (closes: #468501)

 -- Ólafur Jens Sigurðsson <ojsbug@gmail.com>  Fri, 08 Feb 2008 01:03:04 +0100
